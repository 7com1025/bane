package herts.tutorial.com1025;


public class Com1025Application {

    int addTwoNumbers(int number1, int number2) {
        return number1 + number2;
    }

    public int multiplyTwoNumbers(int number1, int number2) {
        return number1 * number2;
    }

    public int minusTwoNumbers(int number1, int number2) {
        return number1 - number2;
    }
}
