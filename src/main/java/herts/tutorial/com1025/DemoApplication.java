package herts.tutorial.com1025;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@SpringBootApplication
public class DemoApplication {

	@GetMapping("/")
	@ResponseBody
	String home() {
		return "Demo App 001";
	}

	@GetMapping("/t")
	@ResponseBody
	String test() {
		return "Demo App 002";
	}

	@GetMapping("/t2")
	@ResponseBody
	String t2Endpoint() {
		return "hello 7com1025";
	}

	@GetMapping("/t3")
	@ResponseBody
	String t3Endpoint() {
		return "hello again 7com1025";
	}

	@GetMapping("/t4")
	@ResponseBody
	String t4Endpoint() {
		return "hello from t4: 7com1025";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
