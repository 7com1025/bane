package herts.tutorial.com1025;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class Com1025ApplicationTests {

    @Test
    void addTwoNumberShouldSuccess() {
        Com1025Application application = new Com1025Application();
        int sum = application.addTwoNumbers(1, 2);
        Assertions.assertEquals(3, sum, "Sum of two numbers should match.");
    }

    @Test
    void addTwoNumberDifferentShouldSuccess() {
        Com1025Application application = new Com1025Application();
        int sum = application.addTwoNumbers(4, 2);
        Assertions.assertEquals(6, sum, "Sum of two numbers should match.");
    }

    @Test
    void multiplyTwoNumberDifferentShouldSuccess() {
        Com1025Application application = new Com1025Application();
        int multiple = application.multiplyTwoNumbers(4, 2);
        Assertions.assertEquals(8, multiple, "Multiply two numbers should success.");
    }

    @Test
    void multiplyTwoNumberShouldSuccess() {
        Com1025Application application = new Com1025Application();
        int multiple = application.multiplyTwoNumbers(0, 2);
        Assertions.assertEquals(0, multiple, "Multiply two numbers should success.");
    }

    @Test
    void minusTwoNumberShouldSuccess() {
        Com1025Application application = new Com1025Application();
        int minus = application.minusTwoNumbers(4, 2);
        Assertions.assertEquals(2, minus, "Minus two numbers should success.");
    }

    @Test
    void minusTwoDifferentNumberShouldSuccess() {
        Com1025Application application = new Com1025Application();
        int minus = application.minusTwoNumbers(2, 4);
        Assertions.assertEquals(-2, minus, "Minus two numbers should success.");
    }
}
